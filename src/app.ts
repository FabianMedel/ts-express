import * as dotenv from 'dotenv';
import express from "express";
import cors  from "cors";
import { configPG, configPG2 } from "../ormconfig";
import { Connection, createConnections } from "typeorm";
import swaggerJsDoc from 'swagger-jsdoc';
import * as swagger from 'swagger-express-ts';
import  swaggerUI  from 'swagger-ui-express';
import { options } from "./swagger/swagger";

dotenv.config();

const { env } = process;

class App {

    public app: express.Application;
    public connection: Connection[];
    public swaggerOptions: object;

    public corsOptions: object = { 
        origin: '*',
        optionsSuccesStatus: 200
    };

    constructor(controllers: any[]) {
        this.app = express();
        this.configuration();
        this.initializeTypeORM();
        this.initializeControllers(controllers)
    }

    private configuration(){
        this.app.set('port', env.PORT || 3000);
        this.app.use(express.json());
        this.app.use(cors(this.corsOptions));
        this.swaggerOptions = swaggerJsDoc(options)
        this.app.use('/docs', swaggerUI.serve, swaggerUI.setup(this.swaggerOptions))
    }

    private async initializeTypeORM() {
        const connection: Connection[] = await createConnections([configPG, configPG2]);
        
        if(connection === undefined){
            throw new Error('Error connecting to database');
        }
        this.connection = connection;
    }

    private initializeControllers(controllers: any[]) {
        controllers.forEach((controller) => {
            this.app.use('/', controller.router)
        });
    }

    public listen(){
        this.app.listen(this.app.get('port'), () =>{
            console.log(`Server running on port ${this.app.get('port')}`);
        });
    }
}

export default App;