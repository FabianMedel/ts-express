import App from "./app";
import UsersController from './components/users/users.controller';
import OrderController from './components/orders/orders.controller';
import ItemController from './components/items/items.controller';

const controllers = [
  new UsersController(),
  new OrderController(),
  new ItemController()
];


const app = new App(controllers)

app.listen();

export default app;