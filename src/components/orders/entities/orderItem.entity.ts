import { BaseEntity, Entity, JoinColumn, ManyToOne, OneToOne, PrimaryGeneratedColumn } from "typeorm";
import { Item } from "../../items/entities/item.entity";
import { Order } from "./order.entity";

@Entity('OrderItem')
export class OrderItem extends BaseEntity {

    @PrimaryGeneratedColumn()
    id!: number;

    @ManyToOne(type => Order, order => order.id) @JoinColumn()
    order!: Order;

    @ManyToOne(type => Item) @JoinColumn()
    item!: Item;
}