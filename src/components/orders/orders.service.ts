import { IOrder } from './interfaces/order.interface';
import { Order } from './entities/order.entity';
import { Connection, createConnection, getConnection, Repository} from 'typeorm';
import { configPG2 } from '../../../ormconfig';
import { OrderItem } from './entities/orderItem.entity';
import { Item } from '../items/entities/item.entity';
import { User } from '../users/entities/user.entity';

export class OrdersService {
    public connection : Connection;
    public ordersRepository:  Repository<Order>;
    public orderItemRelationsRepository: Repository<OrderItem>;
    public itemsRepository: Repository<Item>;
    public usersRepository: Repository<User>;

    constructor(
    ){
        this.init();
    }

    public init = async () => {
        this.connection = await createConnection(configPG2);
        this.initRepository();
    }

    public initRepository = async () => {
        this.ordersRepository = getConnection('orders').getRepository(Order);
        this.orderItemRelationsRepository = getConnection('orders').getRepository(OrderItem);
        this.itemsRepository = getConnection('orders').getRepository(Item);
        this.usersRepository = getConnection('users').getRepository(User);
    }
    
    public findAll = async () => {
        try {
            let data = [];
            const count = await this.orderItemRelationsRepository.createQueryBuilder('OrderItem').getCount();
            const limit = 10;
            console.log(count)
            const total = count / limit;

            for (let index = 0; index < total; index++) {
                const startIndex = (index) * limit;
                const endIndex = index  * limit;
                const orders =  await this.getPaginated(index,limit);

                let paginated = {
                    from: startIndex,
                    to: endIndex,
                    current_page: index,
                    prev_page: {},
                    next_page: {},
                    total: total*10,
                    limit: limit,
                    orders: orders,
                };
                if (paginated.to < paginated.total) {
                    paginated.next_page = {
                        page: index + 1,
                        limit,
                    };
                }
                if (paginated.from > 0) {
                    paginated.prev_page = {
                        page: index - 1,
                        limit,
                    };
                }                
                data.push(paginated);
            }
            
            return {
                statusCode: 200,
                data
            }
        } catch (error) {
            return {
                statusCode: 400,
                message: error
            }
        }
    }

    public getPaginated = async (page: number , limit: number) => {
        try {
            return await this.orderItemRelationsRepository.find(
                { 
                    relations: ['order','item'],
                    skip: limit * page,
                    take: limit
                }
            );

        } catch (error) {
            return error;
        }
    }

    public findOne = async (order_id: string) => {
        try {
            const order = await this.orderItemRelationsRepository.findOne(
                { id: parseInt(order_id) },
                { relations: ['order','item'] }
            );
            if(typeof order === 'undefined'){
                return {
                    statusCode: 200,
                    message: 'order_id not exists'
                }
                
            }
            return {
                statusCode: 200,
                order
            }
        } catch (error) {
            return {
                statusCode: 400,
                message: error
            }
        }
    }

    public findOrdersFromUserId = async (user_id: string) => {
        try {
            const userExist = await this.usersRepository.findOne({id: parseInt(user_id)});
            if(!userExist){
                throw new Error("User not exists");
            }
            
            const ordersfromUser = await this.orderItemRelationsRepository.find(
                {
                    relations: ['order','item'],
                    where: { order: { customer_name: userExist.name}}
                }
            );
            
            return {
                statusCode: 200,
                orders: ordersfromUser
            }
        } catch (error) {
            return {
                statusCode: 400,
                message: error
            }
        }
    }

    public findOrdersByStatus = async (status: string) => {
        try {
            const orders = await this.orderItemRelationsRepository.find(
                {
                    relations: ['order','item'],
                    where: { order: { status }}
                }
            );
            return {
                statusCode: 200,
                orders
            }
        } catch (error) {
            return {
                statusCode: 400,
                message: error
            }
        }
    }

    public create = async (order: IOrder) => {
        try {
            const { order_items } = order;

            const userExists = await this.usersRepository.findOne(
                {  name: order.customer_name }
            );
            if(!userExists){
                throw new Error("User not exists");
            }
            
            let itemExists = await this.itemsRepository.findOne(
                { where: { name: order_items.name } }
            );

            if(!itemExists){
                throw new Error("Item not exists on inventary");
            }
            
            const orderExist = await this.orderItemRelationsRepository.findOne(
                { 
                    relations: ['order','item'],
                    where : {
                        order: { 
                            status: 'open' || 'close' || 'cancel',
                            customer_name: order.customer_name
                        },
                        item: {
                            name: itemExists.name
                        },

                    }
                }
            );

            if (!orderExist) {
                
                const createOrder = await this.saveOrder(order, itemExists);

                if (!createOrder) {
                    throw new Error("Create order error");
                }

                const addItemToOrder = await this.orderItemRelationsRepository.save(
                    {
                        order: createOrder,
                        item: itemExists,
                    }
                );
                if (!addItemToOrder){
                    throw new Error("Add item order error");
                }
            } else {
                if(orderExist.order.status === 'open' ){
                    const items = await this.orderItemRelationsRepository.find(
                        { 
                            relations: ['order','item'],
                            where : { order: { status: 'open' } } }
                    )
                    
                    items.forEach(async (item) => {
                        if (item.item.name === itemExists?.name ) {
                            await this.ordersRepository.update({id: orderExist.id},
                                {
                                    total_items: Math.round(orderExist.order.total_items + order.total_items),
                                    vat: Math.round(itemExists.price * (order.vat/100)),
                                    subtotal: Math.round(orderExist.order.total ),
                                    total: Math.round(item.order.total + (itemExists.price * order.total_items + (itemExists.price * (order.vat/100)) ))
                                }
                            );
                        }
                    });                    
                }

                /*if(orderExist.order.status === 'cancel' ) {
                    await this.ordersRepository.delete({id: orderExist.id})
                    await this.saveOrder(order, itemExists);
                }

                if(orderExist.order.status === 'close' ) {
                    await this.ordersRepository.delete({id: orderExist.id})
                    await this.saveOrder(order, itemExists);
                }
                */
            }
         
            return {
                statusCode: 200,
                order
            }

        } catch (error) {
            return {
                statusCode: 400,
                message: error
            }
        }
    }

    public saveOrder = async ( order: IOrder, itemExists: Item) => {
        return await this.ordersRepository.save(
            {
                subtotal: Math.round(itemExists.price),
                vat: Math.round((itemExists.price * (order.vat/100))),
                total: Math.round(itemExists.price * order.total_items + (itemExists.price * (order.vat/100))),
                token: order.token,
                total_items: Math.round(order.total_items),
                customer_name: order.customer_name,
                status: order.status,
            }
        );
    }


    public updateStatus = async ( { status } : IOrder , order_id: string) =>{
        try {
            await this.ordersRepository.update(order_id, {status: status});
            return {
                statusCode: 200,
            }
        } catch (error) {
            return {
                statusCode: 400,
                message: error
            }
        }
    }

    public delete = async (order_id: string) => {
        try {
            const relationIO = await this.orderItemRelationsRepository.findOne({
                relations: ['order','item'],
                where : { order: { id: order_id} } 
            });
            await this.orderItemRelationsRepository.delete({
                order: { id: relationIO?.order.id}
            })
            await this.ordersRepository.delete(order_id);
            return {
                statusCode: 200,
            }
        } catch (error) {
            return {
                statusCode: 400,
                message: error
            }
        }
    }
   
}
    
