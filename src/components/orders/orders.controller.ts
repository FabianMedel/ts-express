import { Request, Response, Router} from 'express';
import { OrdersService } from './orders.service';


/**
 * @swagger
 * tags:
 *  name: Orders
 *  description: Orders endpoints
 */

class OrderController {

    public path = '/orders';
    public router: Router;
    private ordersService: OrdersService;

    constructor() {
        this.router = Router();
        this.ordersService = new OrdersService();
        this.initializeRoutes();
    }

    public initializeRoutes() {
        
        /**
        * @swagger
        * /orders:
        *  get:
        *    summary: Returns a list of order
        *    tags: [Orders]
        *    responses:
        *      200:
        *        description: the list of orders
        *        content:
        *          application/json:
        *            schema:
        *              type: array
        *              items:
        *                $ref: '#/components/schemas/Order'
        */

        this.router.get(this.path, this.findAll);

        /** 
        * @swagger
        * /orders/{order_id}:
        *  get:
        *    summary: get a order by order Id
        *    tags: [Orders]
        *    parameters:
        *      - $ref: '#/components/parameters/orderId'
        *    responses:
        *      200:
        *        description: The Found Orders
        *        content:
        *          application/json:
        *            schema:
        *            $ref: '#/components/schemas/Order'
        */

        this.router.get(this.path + '/:order_id', this.findOne);

        /** 
        * @swagger
        * /orders/{user_id}/find-by-userId:
        *  get:
        *    summary: get a order by user Id
        *    tags: [Orders]
        *    parameters:
        *      - $ref: '#/components/parameters/userId'
        *    responses:
        *      200:
        *        description: The Found Orders
        *        content:
        *          application/json:
        *            schema:
        *            $ref: '#/components/schemas/Order'
        */

        this.router.get(this.path + '/:user_id/find-by-userId', this.findOrdersFromUserId);

        /** 
        * @swagger
        * /orders/{status}/find-by-status:
        *  get:
        *    summary: get a order by status DELETE
        *    tags: [Orders]
        *    parameters: 
        *     - in: path
        *       name: status
        *       required: true
        *       schema:
        *           type: string
        *    responses:
        *      200:
        *        description: The Found Orders
        *        content:
        *          application/json:
        *            schema:
        *            $ref: '#/components/schemas/Order'
        */

        this.router.get(this.path + '/:status/find-by-status', this.findOrdersByStatus);
        
        /**
        * @swagger
        * /orders:
        *  post:
        *    summary: create a new order
        *    tags: [Orders]
        *    requestBody:
        *      required: true
        *      content:
        *        application/json:
        *          schema:
        *            type: object
        *            properties: 
        *               order_items: 
        *                   type: object
        *                   properties:
        *                       name:
        *                           type: string
        *                       price: 
        *                           type: number
        *               vat: 
        *                   type: number
        *               token: 
        *                   type: string
        *               total_items: 
        *                   type: number
        *               customer_name: 
        *                   type: string
        *               status: 
        *                   type: string
        *                   enum: [open, close, pending, delete]
        *                       
        *
        *    responses:
        *      200:
        *        description: the order was successfully created
        *        content:
        *          application/json:
        *            schema:
        *              $ref: '#/components/schemas/Order'
        *
        */

        this.router.post(this.path, this.create);

        /**
        * @swagger
        * /orders/{order_id}/status:
        *  put:
        *    summary: Update status a order by order id
        *    tags: [Orders]
        *    parameters: 
        *     - in: path
        *       name: order_id
        *       required: true
        *       schema:
        *           type: number
        *    requestBody:
        *      required: true
        *      content:
        *        application/json:
        *          schema:
        *            type: object
        *            properties:
        *               status:
        *                   type: string
        *                   enum: [open, close, pending, delete]
        *    responses:
        *      200:
        *        description: The updated order
        *        content:
        *          application/json:
        *            schema:
        *            $ref: '#/components/schemas/Order'
        *
        */

        this.router.put(this.path + '/:order_id/status', this.updateStatus);

         /**
        * @swagger
        * /orders/{order_id}:
        *  delete:
        *    summary: delete a order by id
        *    tags: [Orders]
        *    parameters:
        *      - $ref: '#/components/parameters/orderId'
        *    responses:
        *      200:
        *        description: the user was deleted
        *        content:
        *          application/json:
        *            schema:
        *            $ref: '#/components/schemas/Order'
        *
        */

        this.router.delete(this.path + '/:order_id', this.delete);
    }

    public findAll = async (req: Request, res: Response) => {    
        res.send(await this.ordersService.findAll())
    }

    public findOne = async (req: Request, res: Response) => {   
        res.send(await this.ordersService.findOne(req.params.order_id));
    }
    
    public  findOrdersFromUserId = async (req: Request, res: Response) => {   
        res.send(await this.ordersService.findOrdersFromUserId(req.params.user_id));
    }

    public findOrdersByStatus = async (req: Request, res: Response) => {
        res.send(await this.ordersService.findOrdersByStatus(req.params.status));
    }

    public create = async (req: Request, res: Response) => {
       res.send(await this.ordersService.create(req.body));
    }

    public updateStatus = async (req: Request, res: Response) => {
        res.send(await this.ordersService.updateStatus(req.body, req.params.order_id));
    }

    public delete = async (req: Request, res: Response) => {
        res.send(await this.ordersService.delete(req.params.order_id));
    }

}

export default OrderController;