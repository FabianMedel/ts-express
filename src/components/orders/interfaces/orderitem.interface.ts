import { IItem } from "../../items/interfaces/item.interface";
import { IOrder } from "./order.interface";


export interface IOrderItem {
    item: IItem;
}