import { Item } from "../../items/entities/item.entity";

export interface IOrder {
    order_items: Item;
    subtotal?: number;
    vat: number;
    total?: number;
    token: string;
    total_items: number;
    customer_name: string;
    status: 'open'|'pending'|'close'|'delete';
}


