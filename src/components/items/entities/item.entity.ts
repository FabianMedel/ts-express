import { BaseEntity, Column, Entity, PrimaryGeneratedColumn } from "typeorm";


@Entity('Item')
export class Item extends BaseEntity {

    @PrimaryGeneratedColumn()
    id!: number;

    @Column()
    name!: string;
    
    @Column()
    price!: number;

}