import { IItem } from './interfaces/item.interface';
import { Item } from './entities/item.entity';
import { Connection, createConnection, Repository} from 'typeorm';
import { configPG2 } from '../../../ormconfig';

export class ItemsService {
    private connection : Connection;
    public itemsRepository:  Repository<Item>;

    constructor(
    ){
        this.init();
    }

    public init = async () => {
        this.connection = await createConnection(configPG2);
        this.initRepository(this.connection);
    }

    public initRepository = async (connection: Connection) => {
        this.itemsRepository = connection.getRepository(Item);
    }

    public findAll = async () => {
        try {
            let data = [];
            const count = await this.itemsRepository.createQueryBuilder('Item').getCount();
            const limit = 10;
            const total = count / limit;
            for (let index = 0; index < total ; index++) {
                const startIndex = (index) * limit;
                const endIndex = index  * limit;
                const items =  await this.getPaginated(index,limit);

                let paginated = {
                    from: startIndex,
                    to: endIndex,
                    current_page: index,
                    prev_page: {},
                    next_page: {},
                    total: total*10,
                    limit: limit,
                    items: items,
                };
                if (paginated.to < paginated.total) {
                    paginated.next_page = {
                        page: index + 1,
                        limit,
                    };
                }
                if (paginated.from > 0) {
                    paginated.prev_page = {
                        page: index - 1,
                        limit,
                    };
                }                
                data.push(paginated);
            }
            
            return {
                statusCode: 200,
                data
            }
        } catch (error) {
            return {
                statusCode: 400,
                message: error
            }
        }
    }

    public getPaginated = async (page: number , limit: number) => {
        try {
            return await this.itemsRepository
                .createQueryBuilder('Item')
                .skip(limit * page)
                .take(limit)
                .getMany();

        } catch (error) {
            return error;
        }
    }

    public findOne = async (item_id: string) => {
        try {
            const item = await this.itemsRepository.findOne({id: parseInt(item_id)})
            if(typeof item === 'undefined'){
                return {
                    statusCode: 200,
                    message: 'item_id not exists'
                }
                
            }
            return {
                statusCode: 200,
                item
            }
        } catch (error) {
            return {
                statusCode: 400,
                message: error
            }
        }
    }

    public create = async (item: IItem) =>{
        try {
            const itemExists = await this.itemsRepository.findOne({name: item.name});
            if(itemExists){
                throw new Error("Items exists");
            }
            const itemCreate = await this.itemsRepository.save(item);
            return {
                statusCode: 200,
                item: itemCreate
            }
        } catch (error) {
            return {
                statusCode: 400,
                message: error
            }
        }
    }

    public update = async (item: IItem, item_id: string) =>{
        try {
            await this.itemsRepository.update(item_id, {
                name: item.name,
                price: Number(item.price)
            });
            return {
                statusCode: 200,
            }
        } catch (error) {
            return {
                statusCode: 400,
                message: error
            }
        }
    }

    public delete = async (item_id: string) => {
        try {
            await this.itemsRepository.delete(item_id);
            return {
                statusCode: 200,
            }
        } catch (error) {
            return {
                statusCode: 400,
                message: error
            }
        }
    }
   
}
    
