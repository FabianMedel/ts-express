import express, { Request, Response, Router} from 'express';
import { ItemsService } from './items.service';


/**
 * @swagger
 * tags:
 *  name: Items
 *  description: Items endpoints
 */

class ItemController {

    public path = '/items';
    public router: Router;
    private itemsService: ItemsService;

    constructor() {
        this.router = Router();
        this.itemsService = new ItemsService();
        this.initializeRoutes();
    }

    public initializeRoutes() {
        
        /**
        * @swagger
        * /items:
        *  get:
        *    summary: Returns a list of items
        *    tags: [Items]
        *    responses:
        *      200:
        *        description: the list of items
        *        content:
        *          application/json:
        *            schema:
        *              type: array
        *              items:
        *                $ref: '#/components/schemas/Item'
        */

        this.router.get(this.path + '/', this.findAll);

        /** 
        * @swagger
        * /items/{item_id}:
        *  get:
        *    summary: get a item by Id
        *    tags: [Items]
        *    parameters:
        *      - $ref: '#/components/parameters/itemId'
        *    responses:
        *      200:
        *        description: The Found Task
        *        content:
        *          application/json:
        *            schema:
        *            $ref: '#/components/schemas/Item'
        */

        this.router.get(this.path + '/:item_id', this.findOne);

        /**
        * @swagger
        * /items:
        *  post:
        *    summary: create a new user
        *    tags: [Items]
        *    requestBody:
        *      required: true
        *      content:
        *        application/json:
        *          schema:
        *            type: object
        *            properties:
        *               name:
        *                   type: string
        *                   description: the name of the item
        *               price:
        *                   type: number
        *                   description: the price of the item
        *            example:
        *               name: vino tinto
        *               price: 899
        * 
        *    responses:
        *      200:
        *        description: the tasks was successfully created
        *        content:
        *          application/json:
        *            schema:
        *              $ref: '#/components/schemas/Item'
        *
        */

        this.router.post(this.path + '/', this.create);

         /**
        * @swagger
        * /items/{item_id}:
        *  put:
        *    summary: Update a user by id
        *    tags: [Items]
        *    parameters:
        *      - $ref: '#/components/parameters/itemId'
        *    requestBody:
        *      required: true
        *      content:
        *        application/json:
        *          schema:
        *            $ref: '#/components/schemas/Item'
        *    responses:
        *      200:
        *        description: The updated user
        *        content:
        *          application/json:
        *            schema:
        *            $ref: '#/components/schemas/Item'
        *
        */

        this.router.put(this.path + '/:item_id', this.update);

         /**
        * @swagger
        * /items/{item_id}:
        *  delete:
        *    summary: delete a item by id
        *    tags: [Items]
        *    parameters:
        *      - $ref: '#/components/parameters/itemId'
        *    responses:
        *      200:
        *        description: the user was deleted
        *        content:
        *          application/json:
        *            schema:
        *            $ref: '#/components/schemas/Item'
        *
        */

        this.router.delete(this.path + '/:item_id', this.delete);
    
    }

    public findAll = async (req: Request, res: Response) => {    
        res.send(await this.itemsService.findAll())
    }

    public findOne = async (req: Request, res: Response) => {   
        res.send(await this.itemsService.findOne(req.params.item_id));
    }
    
    public create = async (req: Request, res: Response) => {
       res.send(await this.itemsService.create(req.body));
    }

    public update = async (req: Request, res: Response) => {
        res.send(await this.itemsService.update(req.body, req.params.item_id));
    }

    public delete = async (req: Request, res: Response) => {
        res.send(await this.itemsService.delete(req.params.item_id));
    }

}

export default ItemController;