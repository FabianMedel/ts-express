import { Request, Response, Router} from 'express';
import { ApiPath } from 'swagger-express-ts';
import { UsersService } from './users.service';
import swaggerUI from "swagger-ui-express";


/**
 * @swagger
 * tags:
 *  name: Users
 *  description: Users endpoints
 */

class UsersController {

    public path = '/users';
    public router: Router;
    private usersService : UsersService;

    constructor(
    ) {
        this.router = Router();
        this.usersService = new UsersService();
        this.initializeRoutes();
    }

    public initializeRoutes() {
        
        /**
        * @swagger
        * /users:
        *  get:
        *    summary: Returns a list of users
        *    tags: [Users]
        *    responses:
        *      200:
        *        description: the list of users
        *        content:
        *          application/json:
        *            schema:
        *              type: array
        *              items:
        *                $ref: '#/components/schemas/User'
        */

        this.router.get(this.path + '/', this.findAll);
       
        /** 
        * @swagger
        * /users/{user_id}:
        *  get:
        *    summary: get a task by Id
        *    tags: [Users]
        *    parameters:
        *      - $ref: '#/components/parameters/userId'
        *    responses:
        *      200:
        *        description: The Found Task
        *        content:
        *          application/json:
        *            schema:
        *            $ref: '#/components/schemas/User'
        */

        this.router.get(this.path + '/:user_id', this.findOne);

        /**
        * @swagger
        * /users:
        *  post:
        *    summary: create a new user
        *    tags: [Users]
        *    requestBody:
        *      required: true
        *      content:
        *        application/json:
        *          schema:
        *            type: object
        *            properties:
        *               name:
        *                   type: string
        *                   description: the name of the user
        *            example:
        *               name: ruben
        *    responses:
        *      200:
        *        description: the tasks was successfully created
        *        content:
        *          application/json:
        *            schema:
        *              $ref: '#/components/schemas/User'
        *
        */
        this.router.post(this.path + '/', this.create);


        /**
        * @swagger
        * /users/{user_id}:
        *  put:
        *    summary: Update a user by id
        *    tags: [Users]
        *    parameters:
        *      - $ref: '#/components/parameters/userId'
        *    requestBody:
        *      required: true
        *      content:
        *        application/json:
        *          schema:
        *            $ref: '#/components/schemas/User'
        *    responses:
        *      200:
        *        description: The updated user
        *        content:
        *          application/json:
        *            schema:
        *            $ref: '#/components/schemas/User'
        *
        */
        this.router.put(this.path + '/:user_id', this.update);

        /**
        * @swagger
        * /users/{user_id}:
        *  delete:
        *    summary: delete a user by id
        *    tags: [Users]
        *    parameters:
        *      - $ref: '#/components/parameters/userId'
        *    responses:
        *      200:
        *        description: the user was deleted
        *        content:
        *          application/json:
        *            schema:
        *            $ref: '#/components/schemas/User'
        *
        */
        this.router.delete(this.path + '/:user_id', this.delete);
    }

    public findAll = async (req: Request, res: Response) => {    
        res.send(await this.usersService.findAll())
    }

    public findOne = async (req: Request, res: Response) => {   
        res.send(await this.usersService.findOne(req.params.user_id));
    }
    
    public create = async (req: Request, res: Response) => {
       res.send(await this.usersService.create(req.body));
    }

    public update = async (req: Request, res: Response) => {
        res.send(await this.usersService.update(req.body, req.params.user_id));
    }

    public delete = async (req: Request, res: Response) => {
        res.send(await this.usersService.delete(req.params.user_id));
    }

}

export default UsersController;