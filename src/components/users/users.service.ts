import { IUser } from './interfaces/user.interface';
import { User } from './entities/user.entity';


import { Connection, createConnection, Repository} from 'typeorm';
import { configPG } from '../../../ormconfig';

export class UsersService {
    private connection : Connection;
    public usersRepository:  Repository<User>;

    constructor(
    ){
        this.init();
    }

    public init = async () => {
        this.connection = await createConnection(configPG);
        this.initRepository(this.connection);
    }

    public initRepository = async (connection: Connection) => {
        this.usersRepository = connection.getRepository(User);
    }

    public findAll = async () => {
        try {
            const users = await this.usersRepository.find();
            return {
                statusCode: 200,
                users
            }
        } catch (error) {
            return {
                statusCode: 400,
                message: error
            }
        }
    }

    public findOne = async (user_id: string) => {
        try {
            const user = await this.usersRepository.findOne({id: parseInt(user_id)})
            if(typeof user === 'undefined'){
                return {
                    statusCode: 200,
                    message: 'user_id not exists'
                } 
            }
            return {
                statusCode: 200,
                user
            }
        } catch (error) {
            return {
                statusCode: 400,
                message: error
            }
        }
    }

    public create = async (user: IUser) =>{
        try {
            const users = await this.usersRepository.save(user);
            return {
                statusCode: 200,
                user
            }
        } catch (error) {
            return {
                statusCode: 400,
                message: error
            }
        }
    }

    public update = async (user: IUser, user_id: string) =>{
        try {
            await this.usersRepository.update(user_id, user);
            return {
                statusCode: 200,
            }
        } catch (error) {
            return {
                statusCode: 400,
                message: error
            }
        }
    }

    public delete = async (user_id: string) => {
        try {
            await this.usersRepository.delete(user_id);
            return {
                statusCode: 200,
            }
        } catch (error) {
            return {
                statusCode: 400,
                message: error
            }
        }
    }
   
}
    
