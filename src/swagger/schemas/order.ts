
/**
 * @swagger
 * components:
 *      schemas:
 *          Order:
 *              type: object
 *              properties:
 *                  from:
 *                      type: number
 *                      description: index inicial
 *                  to:
 *                      type: number
 *                      description: index final
 *                  current_page:
 *                      type: number
 *                      description: pagina actual
 *                  prev_page:
 *                      type: object
 *                      properties:
 *                          page:
 *                              type: number
 *                              description: pagina actual
 *                          limit:
 *                              type: number
 *                              description: limite de pagina
 *                  next_page:
 *                      type: object
 *                      properties:
 *                          page:
 *                              type: number
 *                              description: pagina siguiente
 *                          limit:
 *                              type: number
 *                              description: limite de pagina
 *                  total:
 *                      type: number
 *                      description: total de paginas
 *                  limit:
 *                      type: number
 *                      description:  limite de ordenes
 *                  order: 
 *                      type: array
 *                      items:
 *                          type: object
 *                          properties:
 *                              id: 
 *                                  type: number
 *                                  description: order id
 *                              subtotal: 
 *                                  type: number
 *                                  description: subtotal (precio de producto * unidades)
 *                              vat:
 *                                  type: number
 *                                  description: vat for item
 *                              total:
 *                                  type: number
 *                                  description: vat + item price
 *                              token:
 *                                  type: string
 *                                  description: token for order
 *                              total_items:
 *                                  type: number
 *                                  description: total items 
 *                              customer_name:
 *                                  type: string
 *                                  description: customer name
 *                              status:
 *                                  type: string
 *                                  enum: [open, pending, close, delete]
 *                  item: 
 *                      type: array
 *                      items:
 *                          type: object
 *                          properties:
 *                              id: 
 *                                  type: number
 *                                  description: item id
 *                              name: 
 *                                  type: string
 *                                  description: otem name
 *                              price:
 *                                  type: number
 *                                  description: price item
 * 
 *      parameters:
 *          orderId:
 *              in: path
 *              name: order_id
 *              required: true
 *          schema:
 *              type: number
 *              description: the order id
 *          
 */
