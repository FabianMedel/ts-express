
/**
 * @swagger
 * components:
 *      schemas:
 *          User:
 *              type: object
 *              properties:
 *                  id:
 *                      type: number
 *                      description: the auto-generated id of user
 *                  name:
 *                      type: string
 *                      description: the name of the user
 *              example:
 *                  id: 1
 *                  name: ruben
 * 
 *      parameters:
 *          userId:
 *              in: path
 *              name: user_id
 *              required: true
 *          schema:
 *              type: number
 *              description: the user id
 */