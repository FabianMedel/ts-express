
/**
 * @swagger
 * components:
 *      schemas:
 *          Item:
 *              type: object
 *              properties:
 *                  from:
 *                      type: number
 *                      description: index inicial
 *                  to:
 *                      type: number
 *                      description: index final
 *                  current_page:
 *                      type: number
 *                      description: pagina actual
 *                  prev_page:
 *                      type: object
 *                      properties:
 *                          page:
 *                              type: number
 *                              description: pagina actual
 *                          limit:
 *                              type: number
 *                              description: limite de pagina
 *                  next_page:
 *                      type: object
 *                      properties:
 *                          page:
 *                              type: number
 *                              description: pagina siguiente
 *                          limit:
 *                              type: number
 *                              description: limite de pagina
 *                  total:
 *                      type: number
 *                      description: total de paginas
 *                  limit:
 *                      type: number
 *                      description:  limite de ordenes
 *                  item:
 *                      type: array
 *                      items:
 *                          type: object
 *                          properties:
 *                              id:
 *                                  type: number
 *                                  description: the auto-generated id of item
 *                              name:
 *                                  type: string
 *                                  description: the name of the item
 *                              price:
 *                                  type: number
 *                                  description: the price of the item
 *
 *      parameters:
 *          itemId:
 *              in: path
 *              name: item_id
 *              required: true
 *          schema:
 *              type: number
 *              description: the item id
 */