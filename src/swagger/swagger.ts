export const options = {
    definition: {
        openapi: "3.0.0",
        info: {
            description: "Este es un microservicio como prueba tècnica",
            version: "1.0.0",
            title: "Umvel API",
            contact: {
                email: "fabianmedelra@gmail.com"
            },
        },
        servers: [
            {
                url: "http://localhost:3000"
            }
        ]
    },
    apis: [
            "./src/components/**/*.ts", 
            "./src/swagger/schemas/*.ts"
        ],
}
