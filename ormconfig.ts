import * as dotenv from 'dotenv';
import { ConnectionOptions } from "typeorm";
import { join } from "path";
import { User } from "./src/components/users/entities/user.entity";
import { Order } from "./src/components/orders/entities/order.entity";
import { OrderItem } from "./src/components/orders/entities/orderItem.entity";
import { Item } from "./src/components/items/entities/item.entity";

dotenv.config();

const { env } = process;

const configPG: ConnectionOptions = {
    type: 'postgres',
    host: env.HOST_DB1 || 'postgres1',
    port: Number(env.PORT_DB1) || 5432,
    username: env.USER_NAME_DB1 || 'postgres',
    password: env.PASS_DB1 || 'postgres',
    database: env.DB1_NAME || 'umvel_receptions',
    entities: [User],
    synchronize: true,
    logging: true,
    name: 'users'
}
const configPG2: ConnectionOptions = {
    type: 'postgres',
    host: env.HOST_DB2 || 'postgres2',
    port: Number(env.PORT_DB2) || 5433,
    username: env.USER_NAME_DB2 || 'postgres',
    password: env.PASS_DB2 || 'postgres',
    database:  env.DB2_NAME || 'umvel_orders',
    entities: [Order,OrderItem,Item],
    synchronize: true,
    logging: true,
    name: 'orders'
}

export { configPG, configPG2 }